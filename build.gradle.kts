plugins {
    id("com.xenoterracide.gradle.java-lib").version("0.3.9")
}

group = "com.xenoterracide"
version = "0.1.4-SNAPSHOT"
// In this section you declare the dependencies for your production and test code
dependencies {
    compileOnly("org.immutables:value")
    implementation("org.slf4j:slf4j-api")
    implementation("com.google.guava:guava:latest.release")
    annotationProcessor("org.immutables:value")
    annotationProcessor("org.immutables:builder")
    testCompileOnly("org.immutables:value")
    testImplementation("org.mockito:mockito-core")
}

spotbugs {
    excludeFilter = file("config/spotbugs/exclude.xml")
}
