package com.xenoterracide.util.exception;


import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class TryTest {

    private boolean fromStringThrows( String string ) {
        throw new RuntimeException();
    }

    private boolean fromString( String string ) {
        return Boolean.valueOf( string );
    }

    @Test
    void functionJustOnSuccess() {
        assertThat( TryFunction.attempt( this::fromString ).apply( "true" ) ).isTrue();
    }

    @Test
    void functionOnCatchDefault() {
        assertThat( TryFunction.attempt( this::fromStringThrows ).apply( "true" ) ).isNull();
    }

    @Test
    void functionOnCatch() {
        @SuppressWarnings( "unchecked" )
        Consumer<Throwable> consumer = mock( Consumer.class );
        assertThat( TryFunction.attempt( this::fromStringThrows ).exceptionHandler( consumer ).apply( "true" ) )
            .isNull();
        verify( consumer ).accept( isA( Throwable.class ) );
    }

    @Test
    void functionOrElse() {
        assertThat( TryFunction.attempt( this::fromStringThrows ).defaultValue( true ).apply( "false" ) ).isTrue();
    }


    @Test
    void functionStreamFilter() {
        @SuppressWarnings( "unchecked" )
        Consumer<Throwable> consumer = mock( Consumer.class );
        assertThat(
            Stream.of( "true" )
                .filter( TryPredicate.attempt( s -> false ).exceptionHandler( consumer ) )
                .collect( Collectors.toList() ) )
            .isEmpty();

        assertThat( Stream.of( "true" )
            .filter( TryPredicate.attempt( s -> { throw new Exception(); } ).exceptionHandler( consumer ) )
            .collect( Collectors.toList() )
        ).isEmpty();

        verify( consumer, times( 1 ) ).accept( isA( Throwable.class ) );
    }

    @Test
    void predicateJustOnSuccess() {
        assertThat( TryPredicate.attempt( this::fromString ).test( "true" ) ).isTrue();
    }

    @Test
    void predicateOnCatchDefault() {
        assertThat( TryPredicate.attempt( this::fromStringThrows ).test( "true" ) ).isFalse();
    }

    @Test
    void predicateOnCatch() {
        Consumer<Throwable> consumer = mock( Consumer.class );
        assertThat( TryPredicate.attempt( this::fromStringThrows ).exceptionHandler( consumer ).test( "true" ) )
            .isFalse();
        verify( consumer ).accept( isA( Throwable.class ) );
    }

    @Test
    void predicateOrElse() {
        assertThat( TryPredicate.attempt( this::fromStringThrows ).defaultValue( true ).test( "false" ) ).isTrue();
    }


    @Test
    void predicateStreamFilter() {
        assertThat( Stream.of( "true" ).filter( TryPredicate.attempt( this::fromString ) ) ).containsExactly( "true" );
    }
}
