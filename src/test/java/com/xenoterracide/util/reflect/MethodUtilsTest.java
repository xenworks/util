package com.xenoterracide.util.reflect;


import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class MethodUtilsTest {

    @Test
    void testInvoke() {
        Foo foo = new Foo();
        Method getRet = ClassUtils.getMethod( foo, "getRet" );
        Bar bar = MethodUtils.invoke( Bar.class, foo, getRet );

        assertThat( bar ).isNotNull();
    }

    private static class Foo {
        Bar getRet() {
            return new Bar();
        }
    }

    private static class Bar {

    }
}
