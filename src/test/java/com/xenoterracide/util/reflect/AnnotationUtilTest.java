package com.xenoterracide.util.reflect;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.api.Test;

import static com.xenoterracide.util.reflect.AnnotationUtils.containsAny;
import static com.xenoterracide.util.reflect.AnnotationUtils.hasAnnotation;
import static org.assertj.core.api.Assertions.assertThat;


class AnnotationUtilTest {

    @Test
    void testContainsAny() {
        assertThat( containsAny( MyTest.class, Test.class, MyAnnotation.class ) ).isTrue();
        assertThat( containsAny( MyTest.class, Test.class, Override.class ) ).isFalse();
    }

    @Test
    void testHasAnnotation() {
        assertThat( hasAnnotation( MyTest.class, MyAnnotation.class ) ).isTrue();
        assertThat( hasAnnotation( MyTest.class, Test.class ) ).isFalse();
    }

    @Target( ElementType.TYPE )
    @Inherited
    @Retention( RetentionPolicy.RUNTIME )
    @interface MyAnnotation {
    }

    @MyAnnotation
    private static class MyTest {
    }
}
