package com.xenoterracide.util.reflect;


import org.junit.jupiter.api.Test;

import static com.xenoterracide.util.reflect.ClassUtils.isAssignableFrom;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class ClassUtilsTest {

    @Test
    void testIsAssignableFrom() {

        String t1 = "test";

        assertTrue( isAssignableFrom( t1, CharSequence.class ) );
        assertFalse( isAssignableFrom( t1, Integer.class ) );
    }
}
