package com.xenoterracide.util;


import java.io.OutputStream;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class FilesTest {

    @Test
    void createTempOutputStream() {
        OutputStream txt = Files.newTempOutputStream( this.getClass(), "txt" );
        assertThat( txt ).isNotNull();
    }

}
