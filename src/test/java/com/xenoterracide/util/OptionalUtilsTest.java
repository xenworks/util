package com.xenoterracide.util;


import java.util.Optional;

import org.junit.jupiter.api.Test;

import static com.xenoterracide.util.OptionalUtils.equal;
import static com.xenoterracide.util.OptionalUtils.get;
import static org.assertj.core.api.Assertions.assertThat;

class OptionalUtilsTest {

    @Test
    void testEquals() {
        assertThat( equal( Optional.of( "a" ), Optional.of( "a" ) ) ).isTrue();
        assertThat( equal( "a", Optional.of( "a" ) ) ).isTrue();
        assertThat( equal( Optional.of( "a" ), "a" ) ).isTrue();
        assertThat( equal( "a", "a" ) ).isTrue();
    }

    @Test
    void testGet() {

        assertThat( OptionalUtils.<String>get( Optional.empty() ) ).isNull();
        assertThat( get( Optional.of( "a" ) ) ).contains( "a" );
    }
}
