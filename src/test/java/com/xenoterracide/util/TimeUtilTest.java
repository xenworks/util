package com.xenoterracide.util;


import java.time.Instant;
import java.util.Date;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class TimeUtilTest {

    @Test
    void testOptionalInstantFromDateWithNull() {

        Optional<Instant> optional = TimeUtils.optionalInstantFromDate( null );

        assertThat( optional ).isNotPresent();
    }

    @Test
    void testOptionalInstantFromDateWithDate() {

        Optional<Instant> optional = TimeUtils.optionalInstantFromDate( new Date() );

        assertThat( optional ).isPresent().containsInstanceOf( Instant.class );
    }
}
