package com.xenoterracide.util;


import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

import static com.xenoterracide.util.PredicateUtils.not;
import static org.assertj.core.api.Assertions.assertThat;

class PredicateUtilsTest {

    @Test
    void isPredicate() {
        assertThat( not( ( o ) -> true ) ).isInstanceOf( Predicate.class );
    }

    @Test
    void negates() {
        Predicate<Boolean> not = PredicateUtils.not( ( o ) -> o );
        assertThat( not ).accepts( false ).rejects( true );
    }

    @Test
    void methodRef() {
        Stream<Object> stream = Stream.of( null, null ).filter( not( Objects::nonNull ) );
        assertThat( stream.count() ).isEqualTo( 2 );
    }
}
