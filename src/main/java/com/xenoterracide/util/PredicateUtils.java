package com.xenoterracide.util;

import java.util.function.Predicate;

public interface PredicateUtils {

    static <T> Predicate<T> not( Predicate<T> predicate ) {
        return predicate.negate();
    }
}
