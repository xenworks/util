package com.xenoterracide.util.reflect;

import java.lang.reflect.Method;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

import com.google.common.reflect.TypeToken;

/**
 * methods that should be on class
 */
public interface ClassUtils {

    static Method getMethod( Object object, String name, Class<?>... args ) {
        return getMethod( object.getClass(), name, args );
    }

    static Method getMethod( Class<?> clazz, String name, Class<?>... args ) {
        try {
            findMethods( clazz, byName( name ) ).forEach( method -> {
                System.out.println( method.getName() );
                SecurityUtils.makeAccessible( method );
            } );
            return clazz.getDeclaredMethod( name, args );
        }
        catch ( NoSuchMethodException e ) {
            throw new IllegalStateException( e );
        }
    }

    static Stream<Method> findMethods( Class<?> clazz, Predicate<Method> filter ) {
        return findAllMethods( clazz ).filter( filter );
    }

    static Predicate<Method> byName( String name ) {
        return method -> Objects.equals( method.getName(), name );
    }

    static Stream<Method> findAllMethods( Class<?> clazz ) {
        return Stream.concat( Stream.of( clazz.getDeclaredMethods() ), Stream.of( clazz.getMethods() ) );
    }

    static boolean isAssignableFrom( Object o, Class<?> type ) {
        return TypeToken.of( type ).isSupertypeOf( o.getClass() );
    }


}
