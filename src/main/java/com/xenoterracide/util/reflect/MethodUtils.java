package com.xenoterracide.util.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * methods that should be on method
 */
public interface MethodUtils {

    static <T> T invoke( Class<T> ret, Object instance, Method method, Object... args ) {
        try {
            Object o = method.invoke( instance, args );
            return ret.cast( o );
        }
        catch ( ClassCastException | IllegalAccessException | InvocationTargetException e ) {
            throw new IllegalStateException( e );
        }
    }
}
