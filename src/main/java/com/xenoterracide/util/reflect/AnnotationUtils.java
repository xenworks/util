package com.xenoterracide.util.reflect;


import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

public interface AnnotationUtils {

    @SafeVarargs
    static boolean containsAny(
        AnnotatedElement element,
        Class<? extends Annotation>... classes
    ) {
        return containsAny( element, Arrays.asList( classes ) );
    }

    static Boolean containsAny(
        AnnotatedElement element,
        Collection<Class<? extends Annotation>> classes ) {

        return classes.parallelStream().anyMatch( ( clazz ) -> hasAnnotation( element, clazz ) );
    }

    static Boolean hasAnnotation( AnnotatedElement element, Class<? extends Annotation> annotationType ) {
        return Stream.of( element.getAnnotations() )
                .filter( annotation -> annotation.annotationType().equals( annotationType ) )
                .findAny()
                .isPresent();
    }
}
