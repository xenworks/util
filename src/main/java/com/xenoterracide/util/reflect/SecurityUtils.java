package com.xenoterracide.util.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.security.AccessController;
import java.security.PrivilegedAction;

/**
 * makeAccessible take from Spring ReflectionUtils
 */
public interface SecurityUtils {

    /**
     * Make the given field accessible, explicitly setting it accessible if
     * necessary. The {@code setAccessible(true)} method is only called
     * when actually necessary, to avoid unnecessary conflicts with a JVM
     * SecurityManager (if active).
     *
     * @param field the field to make accessible
     * @see java.lang.reflect.Field#setAccessible
     */
    static void makeAccessible( Field field ) {
        if ( ( !Modifier.isPublic( field.getModifiers() )
                || !Modifier.isPublic( field.getDeclaringClass().getModifiers() )
                || Modifier.isFinal( field.getModifiers() ) ) && !field.isAccessible() ) {
            AccessController.doPrivileged( (PrivilegedAction<?>) () -> {
                field.setAccessible( true );
                return null;
            } );
        }
    }

    /**
     * Make the given method accessible, explicitly setting it accessible if
     * necessary. The {@code setAccessible(true)} method is only called
     * when actually necessary, to avoid unnecessary conflicts with a JVM
     * SecurityManager (if active).
     *
     * @param method the method to make accessible
     * @see java.lang.reflect.Method#setAccessible
     */
    static void makeAccessible( Method method ) {
        if ( ( !Modifier.isPublic( method.getModifiers() )
                || !Modifier.isPublic( method.getDeclaringClass().getModifiers() ) ) && !method.isAccessible() ) {
            AccessController.doPrivileged( (PrivilegedAction<?>) () -> {
                method.setAccessible( true );
                return null;
            } );
        }
    }

    /**
     * Make the given constructor accessible, explicitly setting it accessible
     * if necessary. The {@code setAccessible(true)} method is only called
     * when actually necessary, to avoid unnecessary conflicts with a JVM
     * SecurityManager (if active).
     *
     * @param ctor the constructor to make accessible
     * @see java.lang.reflect.Constructor#setAccessible
     */
    static void makeAccessible( Constructor<?> ctor ) {
        if ( ( !Modifier.isPublic( ctor.getModifiers() )
                || !Modifier.isPublic( ctor.getDeclaringClass().getModifiers() ) ) && !ctor.isAccessible() ) {
            AccessController.doPrivileged( (PrivilegedAction<?>) () -> {
                ctor.setAccessible( true );
                return null;
            } );
        }
    }
}
