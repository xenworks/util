package com.xenoterracide.util;

import java.util.Objects;
import java.util.Optional;

public interface OptionalUtils {

    static boolean equal( Object a, Object b ) {
        if ( a instanceof Optional ^ b instanceof Optional ) { // xor
            if ( a instanceof Optional ) {
                return Objects.equals( get( (Optional<?>) a ), b );
            }
            return Objects.equals( a, get( (Optional<?>) b ) );

        }
        return Objects.equals( a, b );
    }

    static <T> T get( Optional<T> optional ) {
        return optional.orElse( null );
    }
}
