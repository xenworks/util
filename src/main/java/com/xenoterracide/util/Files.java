package com.xenoterracide.util;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Files {
    private static final Logger LOG = LoggerFactory.getLogger( Files.class );

    private Files() {
    }

    public static File writeTo( CharSequence text, String path ) throws IOException {
        return writeTo( text, Paths.get( path ) );
    }

    public static File writeTo( CharSequence text, Path path ) {
        Objects.requireNonNull( text );
        Objects.requireNonNull( path );

        File file = path.toFile();
        try {
            com.google.common.io.Files.asCharSink( file, StandardCharsets.UTF_8 ).write( text );
        }
        catch ( IOException e ) {
            LOG.error( "{}", e );
        }
        return file;
    }

    public static Path createTempPath( Class<?> prefix, String extension ) {
        Objects.requireNonNull( prefix );
        Objects.requireNonNull( extension );
        try {
            return java.nio.file.Files.createTempFile( prefix.getSimpleName(), '.' + extension );
        }
        catch ( IOException e ) {
            LOG.error( "{}", e );
            return null;
        }
    }

    public static OutputStream newOutputStream( Path path ) {
        Objects.requireNonNull( path );
        try {
            return java.nio.file.Files.newOutputStream( path );
        }
        catch ( IOException e ) {
            LOG.error( "{}", e );
            return null;
        }
    }

    public static OutputStream newTempOutputStream( Path path ) {
        Objects.requireNonNull( path );
        try {
            return java.nio.file.Files.newOutputStream( path, StandardOpenOption.DELETE_ON_CLOSE );
        }
        catch ( IOException e ) {
            LOG.error( "{}", e );
            return null;
        }
    }

    public static OutputStream newTempOutputStream( Class<?> prefix, String extension ) {
        return newTempOutputStream( createTempPath( prefix, extension ) );
    }
}
