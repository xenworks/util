package com.xenoterracide.util.exception;


import java.util.Optional;
import java.util.function.Consumer;

import org.immutables.value.Value;
import org.slf4j.LoggerFactory;

public interface TryDefinition<R, THROWER> {

    THROWER attempt();

    Optional<R> defaultValue();

    @Value.Default
    default Consumer<Throwable> exceptionHandler() {
        return throwable -> LoggerFactory.getLogger( TryDefinition.class ).error( "", throwable );
    }
}
