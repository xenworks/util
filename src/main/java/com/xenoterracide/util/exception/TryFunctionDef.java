package com.xenoterracide.util.exception;


import java.util.function.Consumer;
import java.util.function.Function;

import org.immutables.value.Value;

@Value.Immutable
@Value.Style(
    builderVisibility = Value.Style.BuilderVisibility.PUBLIC,
    visibility = Value.Style.ImplementationVisibility.PUBLIC,
    typeImmutable = "*",
    strictBuilder = true,
    typeAbstract = "*Def"
)
abstract class TryFunctionDef<T, R> implements TryDefinition<R, TryFunctionDef.FunctionThrower<T, R>> {

    public static <T, R> TryFunction.Builder<T, R> onCatch( Consumer<Throwable> handler ) {
        return TryFunction.<T, R>builder().exceptionHandler( handler );
    }

    public static <T, R> TryFunction.Builder<T, R> attempt( FunctionThrower<T, R> handler ) {
        return TryFunction.<T, R>builder().attempt( handler );
    }

    @FunctionalInterface
    public interface FunctionThrower<T, R> {
        R apply( T t ) throws Throwable;
    }

    abstract static class Builder<T, R> implements Function<T, R> {
        @Override
        public R apply( T t ) {
            TryFunction<T, R> impl = build();
            try {
                return impl.attempt().apply( t );
            }
            catch ( Throwable e ) {
                impl.exceptionHandler().accept( e );
                return impl.defaultValue().orElse( null );
            }
        }

        abstract TryFunction<T, R> build();
    }
}
