package com.xenoterracide.util.exception;


import java.util.function.Consumer;
import java.util.function.Predicate;

import org.immutables.value.Value;

@Value.Immutable
@Value.Style(
    builderVisibility = Value.Style.BuilderVisibility.PUBLIC,
    visibility = Value.Style.ImplementationVisibility.PUBLIC,
    typeImmutable = "*",
    strictBuilder = true,
    typeAbstract = "*Def"
)
abstract class TryPredicateDef<T> implements TryDefinition<Boolean, TryPredicateDef.PredicateThrower<T>> {

    public static <T> TryPredicate.Builder<T> onCatch( Consumer<Throwable> handler ) {
        return TryPredicate.<T>builder().exceptionHandler( handler );
    }

    public static <T> TryPredicate.Builder<T> attempt( PredicateThrower<T> thrower ) {
        return TryPredicate.<T>builder().attempt( thrower );
    }

    @FunctionalInterface
    public interface PredicateThrower<I> {
        boolean test( I input ) throws Throwable;
    }

    abstract static class Builder<T> implements Predicate<T> {
        @Override
        public boolean test( T t ) {
            TryPredicate<T> impl = build();
            try {
                return impl.attempt().test( t );
            }
            catch ( Throwable e ) {
                impl.exceptionHandler().accept( e );
                return impl.defaultValue().orElse( false );
            }
        }

        abstract TryPredicate<T> build();
    }
}
