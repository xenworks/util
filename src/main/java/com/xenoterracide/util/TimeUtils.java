package com.xenoterracide.util;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

public interface TimeUtils {

    static Optional<Instant> optionalInstantFromDate( Date date ) {
        return Optional.ofNullable( date ).map( Date::toInstant );
    }
}
